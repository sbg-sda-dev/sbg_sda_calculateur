import 'package:flutter/material.dart';
import 'package:sbg_sda_calculateur/views/Home.dart';
import 'package:sbg_sda_calculateur/views/parametre.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Text title;
  final AppBar appBar;
  final bool isHome;

  const BaseAppBar(
      {Key? key,
      required this.title,
      required this.appBar,
      this.isHome = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: title,
      actions: <Widget>[
        Row(
          children: [
            if (!isHome)
              IconButton(
                  icon: Icon(Icons.home),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home()));
                  }),
            IconButton(
                icon: Icon(Icons.settings),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ParametreScreen()));
                })
          ],
        )
      ],
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}

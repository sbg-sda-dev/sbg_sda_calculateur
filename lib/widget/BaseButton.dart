import 'package:flutter/material.dart';

class ButtonImage extends StatelessWidget {
  final Text title;
  final Image image;
  final VoidCallback code;

  const ButtonImage(
      {required Key key,
      required this.title,
      required this.image,
      required this.code})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(onPressed: code, child: Column());
  }
}

class RaisedGradientButton extends StatelessWidget {
  final Widget child;
  final Gradient gradient;
  final double width;
  final double height;
  final Function onPressed;

  const RaisedGradientButton({
    required Key key,
    required this.child,
    required this.gradient,
    this.width = double.infinity,
    this.height = 50.0,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 50.0,
      decoration: BoxDecoration(gradient: gradient, boxShadow: [
        BoxShadow(
          color: Colors.blueGrey,
          offset: Offset(0.0, 1.5),
          blurRadius: 1.5,
        ),
      ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: onPressed as void Function(),
            child: Center(
              child: child,
            )),
      ),
    );
  }
}

//https://flutter.dev/docs/development/data-and-backend/json

///figurine du seigneur des anneaux
///
/// C Combat : Valeur de combat au corp à corp (defaut : 3)
class Figurine {
  //<fields>
  int _combat = 3, _combatTir = 3, _force = 3, _attaque = 3, _defense = 3;

  // ignore: unused_field
  String _nom = "", _catForce = "", _faction = "";

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  String get nom => _nom;
  // ignore: unnecessary_getters_setters
  set nom(String nom) {
    _nom = nom;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  String get catForce => _catForce;
  // ignore: unnecessary_getters_setters
  set catForce(String catForce) {
    _catForce = catForce;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  String get faction => _faction;
  // ignore: unnecessary_getters_setters
  set faction(String faction) {
    _faction = faction;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  int get combat => _combat;
  // ignore: unnecessary_getters_setters
  set combat(int combat) {
    _combat = combat;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  int get combatTir => _combatTir;
  // ignore: unnecessary_getters_setters
  set combatTir(int combatTir) {
    _combatTir = combatTir;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  int get force => _force;
  // ignore: unnecessary_getters_setters
  set force(int force) {
    _force = force;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  int get attaque => _attaque;
  // ignore: unnecessary_getters_setters
  set attaque(int attaque) {
    _attaque = attaque;
  }

  //<getters/setters>
  // ignore: unnecessary_getters_setters
  int get defense => _defense;
  // ignore: unnecessary_getters_setters
  set defense(int defense) {
    _defense = defense;
  }

  //<constructors>
  Figurine(String nom, String catForce, String faction, int combat,
      int combatTir, int force, int attaque, int defense) {
    this._nom = nom;
    this._catForce = catForce;
    this._faction = faction;
    this._combat = combat;
    this._combatTir = combatTir;
    this._force = force;
    this._attaque = attaque;
    this._defense = defense;

    print("Creation de la figurine : " + _nom);
  }

  ///Initialise une Figurine sans nom, ni faction
  Figurine.autogen() {
    this._nom = "";
    this._catForce = "BIEN";
    this._faction = "";
    this._combat = 3;
    this._combatTir = 3;
    this._force = 3;
    this._attaque = 3;
    this._defense = 3;

    print("Creation de la figurine : " + _nom);
  }

  ///Retourne un type Figurine à partir d'un JSON
  factory Figurine.fromJson(dynamic json) {
    return Figurine(
        json['nom'] as String,
        json['catForce'] as String,
        json['faction'] as String,
        json['combat'] as int,
        json['combatTir'] as int,
        json['force'] as int,
        json['attaque'] as int,
        json['defense'] as int);
  }

  ///Retourne un Map<String, dynamic> (JSON) à partir d'une Figurine
  Map<String, dynamic> toJson() => {
        'nom': _nom,
        'catForce': _catForce,
        'faction': _faction,
        'combat': _combat,
        'combatTir': _combatTir,
        'force': _force,
        'attaque': _attaque,
        'defense': _defense,
      };

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'Figurine{nom: $_nom, catForce: $_catForce, faction: $_faction, combat: $_combat, combatTir: $_combatTir, force: $_force, attaque: $_attaque, defense: $_defense}';
  }
}

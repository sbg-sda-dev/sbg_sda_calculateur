import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';

class FigurineStorage {
  Future<File> get _localFile async {
    final directory = await getApplicationDocumentsDirectory();
    return File('${directory.path}/figurine.json');
  }

  //Fonction d'écriture de fichier
  //https://bezkoder.com/dart-flutter-convert-object-to-json-string/#DartFlutter_convert_List_of_objects_to_JSON_string
  Future<File> writeFigurine(List<Figurine> listFigurine) async {
    final file = await _localFile;
    String listJson = '';

    listFigurine.forEach((element) {
      listJson += '${jsonEncode(element)};';
    });

    log('WriteFigurine => $listJson');

    return file.writeAsString('$listJson');
  }

  void resetFigurine() async {
    final file = await _localFile;

    log('RESET FILE FIGURINE');
    //return Liste de STRING -> Parcourir le tableau et convertir la liste de json en list de String
    file.writeAsString('');
  }

  //Fonction de lecture de fichier
  //Recupere une liste de figurine
  Future<List<Figurine>> readFigurine() async {
    List<Figurine> listFigurine = [];
    try {
      final file = await _localFile;

      // Read the file.
      String jsonString = await file.readAsString();
      log('ReadFigurine result lecture => $jsonString');

      List<String> listFigurineJson = jsonString.split(';').toList();
      listFigurineJson.removeLast();
      log('ReadFigurine jsondecode => $listFigurineJson');
      listFigurineJson.forEach((element) {
        log('Read foreach element =>  $element');
        listFigurine.add(Figurine.fromJson(jsonDecode(element)));
      });
    } catch (e) {
      // If encountering an error, return null.
      log('ReadFigurine echec lecture => $e');
      return [];
    }
    log('ReadFigurine return $listFigurine');
    return listFigurine;
  }

  //Ajout d'une figurine
  Future<File> addFigurine(Figurine figurine) async {
    final file = await _localFile;

    String listJson = jsonEncode(figurine);

    log('WriteFigurine add $listJson');
    return file.writeAsString('$listJson;', mode: FileMode.append);
  }
}

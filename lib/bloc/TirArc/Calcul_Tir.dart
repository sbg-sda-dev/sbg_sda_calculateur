import 'dart:math';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/bloc/Global/CalculPourcentage.dart';

const List arrayBlessures = [
  [4, 5, 5, 6, 6, 6, 6, 6, 7, 7],
  [4, 4, 5, 5, 6, 6, 6, 6, 6, 7],
  [3, 4, 4, 5, 5, 6, 6, 6, 6, 6],
  [3, 3, 4, 4, 5, 5, 6, 6, 6, 6],
  [3, 3, 3, 4, 4, 5, 5, 6, 6, 6],
  [3, 3, 3, 3, 4, 4, 5, 5, 6, 6],
  [3, 3, 3, 3, 3, 4, 4, 5, 5, 6],
  [3, 3, 3, 3, 3, 3, 4, 4, 5, 5],
  [3, 3, 3, 3, 3, 3, 3, 4, 4, 5],
  [3, 3, 3, 3, 3, 3, 3, 3, 4, 4],
];

calculTir(Figurine attaquant, Figurine cible, bool distDiv, bool mouvTireur,
    {int obstacleDefense = 0, int obstacleLourd = 0, int obstacleLeger = 0}) {
  var _lanceDes = new Random();
  bool _result,
      _resultObstDef = true,
      _resultObstLourd = true,
      _resultObstLeger = true,
      _resultBlessure = true;

  //====test de trajectoire==== [bool result, int value]
  int _d6 = _lanceDes.nextInt(6);

  int _attaquantCombatTir = attaquant.combatTir;
  //si le tireur a bougé et qu'il n'est pas déjà au min de précision on le pénalise
  if (mouvTireur && _attaquantCombatTir < 6) {
    _attaquantCombatTir++;
  }

  //si la valeur de combat est inférieur au lancé de dès le tir est loupé
  bool _resultTrajectoire = (_d6 <= _attaquantCombatTir) ? true : false;
  /*String _affichageTrajectoire =
      "Valeur de combat à $_attaquantCombatTir, le lancé vaut $_d6";
  _result = _result.add(_affichageTrajectoire);
  print(
      "Chance de toucher : attaquant doit faire $_attaquantCombatTir ou plus \n soit $_result %");*/

  //test obstacleDefense
  for (var i = 0; i < obstacleDefense; i++) {
    _d6 = _lanceDes.nextInt(6);
    //double _resultObstDef = pourcentageDes(5);
    //_result = additionPourcentage(_resultObstDef, _result);

    //lancé supérieur ou égal à 5 (regle du jeu)
    _resultObstDef = (_d6 >= 5) ? true : false;
  }

  //test obstacleLourd
  for (var i = 0; i < obstacleLourd; i++) {
    _d6 = _lanceDes.nextInt(6);
    //double _resultObstLourd = pourcentageDes(4);
    //_result = additionPourcentage(_resultObstLourd, _result);
    _resultObstLourd = (_d6 >= 4) ? true : false;
  }

  //test obstacleLeger
  for (var i = 0; i < obstacleLeger; i++) {
    _d6 = _lanceDes.nextInt(6);
    //double _resultObstLeger = pourcentageDes(3);
    //_result = additionPourcentage(_resultObstLeger, _result);

    _resultObstLeger = (_d6 >= 3) ? true : false;
  }
  //print("Chance de passer les obstacles : " + _result.toString());

  //====test de blessures====

  //si la cible est à la moitié de la ditance de tir, la force de l'arc prend  + 1
  int _forceArc =
      distDiv ? attaquant.force.toInt() + 1 : attaquant.force.toInt();

  _d6 = _lanceDes.nextInt(6);
  int _valueBlessure = arrayBlessures[_forceArc][cible.defense.toInt()];
  _resultBlessure = (_d6 >= _valueBlessure) ? true : false;
  print(
      "pour une force d'attaque de $_forceArc, le joueur doit faire $_valueBlessure sur une cible de défense " +
          cible.defense.toString());

  //double _resultBlessure = pourcentageDes(_valueBlessure);
  /*print("Avec une attaque de " +
      attaquant.force.toString() +
      " et une défense de " +
      cible.defense.toString());
  print("tab blessure return " + _valueBlessure.toString());*/
  //_result = additionPourcentage(_resultBlessure, _result);

  _result = (_resultTrajectoire &
          _resultObstDef &
          _resultObstLourd &
          _resultObstLeger &
          _resultBlessure)
      ? true
      : false;
  print("resultatTir = calculTir -> probaTir return = $_result");
  return _result;
}

//==============================================================================
double probabiliteTir(
    Figurine attaquant, Figurine cible, bool distDiv, bool mouvTireur,
    {int obstacleDefense = 0, int obstacleLourd = 0, int obstacleLeger = 0}) {
  double _result = 0;

  //====test de toucher====
  int _attaquantCombatTir = attaquant.combatTir;
  if (mouvTireur && _attaquantCombatTir < 6) {
    _attaquantCombatTir++;
  }

  _result = pourcentageDes(_attaquantCombatTir);
  print(
      "Chance de toucher : attaquant doit faire $_attaquantCombatTir ou plus \n soit $_result %");

  //test obstacleDefense
  for (var i = 0; i < obstacleDefense; i++) {
    double _resultObstDef = pourcentageDes(5);
    _result = additionPourcentage(_resultObstDef, _result);
  }

  //test obstacleLourd
  for (var i = 0; i < obstacleLourd; i++) {
    double _resultObstLourd = pourcentageDes(4);
    _result = additionPourcentage(_resultObstLourd, _result);
  }

  //test obstacleLeger
  for (var i = 0; i < obstacleLeger; i++) {
    double _resultObstLeger = pourcentageDes(3);
    _result = additionPourcentage(_resultObstLeger, _result);
  }
  //print("Chance de passer les obstacles : " + _result.toString());

  //====test de blessures====

  //si la cible est à la moitié de la ditance de tir, la force de l'arc prend  + 1
  int _forceArc =
      distDiv ? attaquant.force.toInt() + 1 : attaquant.force.toInt();
  int _valueBlessure = arrayBlessures[_forceArc][cible.defense.toInt()];
  print(
      "pour une force d'attaque de $_forceArc, le joueur doit faire $_valueBlessure sur une cible de défense " +
          cible.defense.toString());

  double _resultBlessure = pourcentageDes(_valueBlessure);
  /*print("Avec une attaque de " +
      attaquant.force.toString() +
      " et une défense de " +
      cible.defense.toString());
  print("tab blessure return " + _valueBlessure.toString());*/
  _result = additionPourcentage(_resultBlessure, _result);

  print("proba -> calculTir -> probaTir return = " + _result.toString());
  return _result.roundToDouble();
}
/*
https://gitlab.com/Eremanth/armees-de-la-terre-du-milieu-fr/-/raw/master/Grand%20Livre%20des%20Arm%C3%A9es.pdf
https://gitlab.com/Eremanth/armees-de-la-terre-du-milieu-fr/-/raw/master/Grand%20Livre%20de%20R%C3%A8gles.pdf

Test de Trajectoire
Type d’écran Score requis
Fortifications, Créneaux, Fenêtres étroites 5+ 33%
Murs, Rochers, Howdah, Haies, Figurines 4+  50%
Clôtures basses, Buissons, Herbes hautes 3+ 66%

Tirer sur des figurines engagées en combat
Les figurines du Mal peuvent tirer sur une figurine engagée au
combat (les figurines du Bien ne prendront pas le risque de
blesser leur allié). Parce que les combats n’opposent pas vraiment des figurines statiques mais plutôt des guerriers en pleine
action, il y a un risque que le tir touche une autre figurine du
combat.
Pour tirer sur une figurine engagée au combat, le tireur doit
pouvoir voir sa cible. S’il peut la voir, jetez un dé pour toucher
normalement et résolvez les tests sur la trajectoire si nécessaire
en ignorant le combat.
Enfin vous allez faire un test sur la trajectoire spécial : jetez
1D6 pour voir qui dans le combat sera touché. Sur un 1-3, la
figurine alliée la plus près du tireur sera touchée ; sur un 4-6, la
cible originelle sera touchée. 

Se déplacer et tirer
Si une figurine a bougé, elle aura moins de temps pour se préparer à tirer. Une figurine qui souhaite tirer dans le même tour
où elle a bougé (au maximum de la moitié de son mouvement),
souffre d’un malus sur leur valeur de tir qui rend le tir plus dur
à réussir.
Ceci est représenté par un malus de -1 sur la valeur de tir de la
figurine qui a bougé pour le reste du tour. Donc par exemple, si
la figurine a une valeur de tir de 4+ et qu’elle a bougé pendant
ce tour, elle touchera sa cible sur un 5+. Un jet pour toucher qui
donne 6 est toujours une réussite.
Veuillez noter que faire tourner la figurine sur elle-même ne
compte pas comme un mouvement. 

Tableau des Blessures
Défense
Force
  1 2 3 4 5 6   7   8   9   10
1 4 5 5 6 6 6/4 6/5 6/6 - -
2 4 4 5 5 6 6 6/4 6/5 6/6 -
3 3 4 4 5 5 6 6 6/4 6/5 6/6
4 3 3 4 4 5 5 6 6 6/4 6/5
5 3 3 3 4 4 5 5 6 6 6/4
6 3 3 3 3 4 4 5 5 6 6
7 3 3 3 3 3 4 4 5 5 6
8 3 3 3 3 3 3 4 4 5 5
9 3 3 3 3 3 3 3 4 4 5
10 3 3 3 3 3 3 3 3 4 4


============OLD CODE==================

if (attaquant.combatTir >= _d6) {
    print("Le tir passe avec -> d6 = $_d6 valeur de tir de l'archer = " +
        attaquant.combatTir.toString());
  } else {
    print("Le tir echoue avec -> d6 = $_d6 valeur de tir de l'archer = " +
        attaquant.combatTir.toString());
  }

  //test obstacleDefense
  for (var i = 0; i < obstacleDefense; i++) {
    double _resultObstDef = pourcentageDes(5);
    _result = additionPourcentage(_resultObstDef, _result);
  }

  //test obstacleLourd
  for (var i = 0; i < obstacleLourd; i++) {
    double _resultObstLourd = pourcentageDes(4);
    _result = additionPourcentage(_resultObstLourd, _result);
  }

  //test obstacleLeger
  for (var i = 0; i < obstacleLeger; i++) {
    double _resultObstLeger = pourcentageDes(3);
    _result = additionPourcentage(_resultObstLeger, _result);
  }
  //print("Chance de passer les obstacles : " + _result.toString());

  //====test de blessures====
  int _valueBlessure =
      arrayBlessures[attaquant.force.toInt()][cible.defense.toInt()];
  double _resultBlessure = pourcentageDes(_valueBlessure);
  /*print("Avec une attaque de " +
      attaquant.force.toString() +
      " et une défense de " +
      cible.defense.toString());
  print("tab blessure return " + _valueBlessure.toString());*/
  _result = additionPourcentage(_resultBlessure, _result);
  //test précision
  print("Mon attaquant est précis à " + attaquant.PourcentTir().toString());
  //====test de blessures====
}
*/

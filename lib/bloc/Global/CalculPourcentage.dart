double pourcentage(int _partialValue, int _totalValue) {
  double _result = 100 * _partialValue / _totalValue;
  return _result;
}

double pourcentageDes(int _partialValue) {
  double _result = 100 * (7 - _partialValue) / 6;
  return _result;
}

double additionPourcentage(double pourcent1, double pourcent2) {
  double _result = (pourcent1 + pourcent2) / 2;
  return _result;
}

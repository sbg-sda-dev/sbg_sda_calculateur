import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/bloc/persistance/persistanceFigurine.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

class ModifyFigurine extends StatefulWidget {
  final FigurineStorage storage;
  final Figurine figurine;
  ModifyFigurine({Key? key, required this.storage, required this.figurine})
      : super(key: key);
  @override
  _ModifyFigurineState createState() => _ModifyFigurineState();
}

class _ModifyFigurineState extends State<ModifyFigurine> {
  bool isHero = false;

  //---------https://pub.dev/packages/image_picker---------
  File? _image;
  final picker = ImagePicker();
  get child => null;
  Future getImageCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listCompetenceFigurine = [
      Text("C \n (corp à corp)"),
      Text("C \n (tir à l'arc)"),
      Text("F"),
      Text("D"),
      Text("A"),
      Text("PV"),
      Text("B"),
      Text(widget.figurine.combat.toString()),
      Text(widget.figurine.combatTir.toString()),
      Text(widget.figurine.force.toString()),
      Text(widget.figurine.defense.toString()),
      Text(widget.figurine.attaque.toString()),
      TextField(
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          maxLength: 3,
          style: Theme.of(context).textTheme.headline4,
          decoration: InputDecoration(
              labelText: '0',
              //errorText: _numberInputIsValid ? null : 'Please enter an integer!',
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ))),
      Text("0"),
    ];
    //Color _colorBien = Colors.blueGrey[800];
    //Color _colorMal = Colors.blueGrey[800];
    return Scaffold(
        //======================================================AppBar=========================================
        appBar: BaseAppBar(
          title: Text("modifier figurine"),
          appBar: AppBar(),
        ),

        //=============================================BODY================================================================
        body: Column(children: <Widget>[
          ButtonBar(
            alignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.figurine.nom,
              ),
              Text(widget.figurine.faction)
            ],
          ),
          Divider(
            indent: 50,
            endIndent: 50,
          ),
          Container(
            height: 200,
            width: MediaQuery.of(context).size.width * 0.90,
            child: _image!.existsSync()
                ? OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.grey[700]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.photo_camera),
                        Text("ajouter une image")
                      ],
                    ),
                    onPressed: () {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => SimpleDialog(
                                title: Text("mode image"),
                                children: [
                                  TextButton(
                                      onPressed: getImageCamera,
                                      child: const Text("Camera")),
                                  TextButton(
                                      onPressed: getImageGallery,
                                      child: const Text("Gallery"))
                                ],
                              ));
                    },
                  )
                : Image.file(_image!),
          ),
          Divider(
            indent: 50,
            endIndent: 50,
          ),
          Expanded(
              flex: 1,
              child: new GridView.count(
                  //physics: BouncingScrollPhysics(),
                  crossAxisCount: 7,
                  children: listCompetenceFigurine)),
          /*Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("data"),
              Text("data"),
              Text("data"),
              Text("data"),
              Text("data"),
            ],
          ),
          Divider(
            color: Colors.amber,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(""),
              Text("data"),
              Text("data"),
              Text("data"),
              Text("data"),
            ],
          )*/
        ]));
  }
}

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/bloc/TirArc/Calcul_Tir.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

class TirArc extends StatefulWidget {
  final List<Figurine> listFigurine;
  TirArc({
    Key? key,
    required this.listFigurine,
  }) : super(key: key);

  @override
  _TirArcState createState() => _TirArcState();
}

class _TirArcState extends State<TirArc> {
  List<Figurine> listFigurine = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      this.listFigurine = widget.listFigurine;
    });
  }

  static Figurine _attaquant =
      new Figurine("Tir attaquant", "Bien", "MN", 3, 4, 3, 3, 3);
  static Figurine _cible = new Figurine("Cible", "Bien", "MN", 3, 4, 3, 3, 3);
  double _probaTir = probabiliteTir(_attaquant, _cible, false, false);
  bool _tireurMouv = false;
  bool dist = false;
  bool _resultatTir = false;

  void _updateProbaTir() {
    setState(() {
      print(
          "====================UpdateProbaTir = $_probaTir \n tireur vaut = " +
              _attaquant.combat.toString());

      _probaTir = probabiliteTir(_attaquant, _cible, dist, _tireurMouv);
    });
  }

  Row addObstacle(Text _titre) {
    return Row(
      children: [],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //======================================================AppBar=========================================
        appBar: BaseAppBar(
          title: Text("Tir à l'arc"),
          appBar: AppBar(),
        ),

        //=============================================BODY================================================================
        body: Column(
          children: [
            //DropdownButton(items: listFigurine.map((Figurine figurine) {}).toList(),
            /*
            //---------TIREUR--------------
            TextField(
              keyboardType: TextInputType.number,
              decoration:
                  InputDecoration(labelText: "Valeur de combat du tireur"),
              onChanged: (_attCombatTir) => {
                setState(() {
                  _attaquant.combatTir = int.tryParse(_attCombatTir)!;
                  print(
                      "====================UpdateProbaTir = $_probaTir \n tireur vaut = " +
                          _attaquant.combatTir.toString());

                  _probaTir =
                      probabiliteTir(_attaquant, _cible, dist, _tireurMouv);
                })
              },
            ),
            TextField(
                keyboardType: TextInputType.number,
                maxLength: 1,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                decoration:
                    InputDecoration(labelText: "Force de l'arc du tireur"),
                onChanged: (_attArc) => {
                      _attaquant.force = int.tryParse(_attArc)!,
                      _updateProbaTir()
                    }),*/
            //------------------Cible------------------
            TextField(
                keyboardType: TextInputType.number,
                decoration:
                    InputDecoration(labelText: "Valeur de défense de la cible"),
                onChanged: (_cibleDef) => {
                      _cible.defense = int.tryParse(_cibleDef)!,
                      _updateProbaTir()
                    }),
            //------------------Distance-------------------
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("distance entre le tireur et la cible"),
                Checkbox(
                    value: this.dist,
                    onChanged: (bool? value) {
                      setState(() {
                        this.dist = value!;
                        _updateProbaTir();
                      });
                    }),
              ],
            ),
            //----------------Obstacle defense---------------

            //----------------Obstacle lourd---------------

            //----------------Obstacle leger---------------

            //----------------mouvement tireur---------------
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("Mouvement du tireur"),
                Checkbox(
                    value: this._tireurMouv,
                    onChanged: (bool? value) {
                      setState(() {
                        this._tireurMouv = value!;
                        _updateProbaTir();
                      });
                    }),
              ],
            ),
            //--------------------------AFFICHAGE RESULTAT------------------
            Text("$_probaTir % de chance de tuer la cible"),
            FloatingActionButton(
                child: Text("Tirer"),
                onPressed: () {
                  setState(() {
                    log("old resultat tir = $_resultatTir");
                    _resultatTir =
                        calculTir(_attaquant, _cible, dist, _tireurMouv);
                    log("resultat tir = $_resultatTir");
                  });
                }),
            Container(
              alignment: Alignment.centerLeft,
              color: Colors.grey,
              child: Text(_resultatTir.toString()),
            )
          ],
        ));
  }
}

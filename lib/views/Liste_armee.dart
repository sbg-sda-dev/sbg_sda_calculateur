import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/bloc/persistance/persistanceFigurine.dart';
import 'package:sbg_sda_calculateur/constants.dart';
import 'package:sbg_sda_calculateur/views/actionsFight.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

class ListeArmee extends StatefulWidget {
  final FigurineStorage storage;
  ListeArmee({Key? key, required this.storage}) : super(key: key);

  @override
  ListeArmeeScreenState createState() => ListeArmeeScreenState();
}

class ListeArmeeScreenState extends State<ListeArmee> {
  List<Figurine> listFigurine = [];
  List<Figurine> listFigurineCheck = [];
  List<bool> inList = [];

  @override
  void initState() {
    super.initState();
    widget.storage.readFigurine().then((value) {
      setState(() {
        this.listFigurine += value;
        this.inList = List.generate(this.listFigurine.length, (index) => false);
      });
      log('Gestion de figurine -> value -> $value');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //======================================================AppBar=========================================
      appBar: BaseAppBar(
        title: Text("Liste d'armée"),
        appBar: AppBar(),
      ),

      //=======================================Body==================
      body: ListView.builder(
          itemCount: listFigurine.length,
          itemBuilder: (context, index) {
            return Container(
                padding: EdgeInsets.all(defaultPadding),
                margin: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                //decoration:
                //    BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 2, color: primaryColor.withOpacity(0.15)),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(defaultPadding),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(child: Icon(Icons.account_circle)),
                    Expanded(
                        flex: 2,
                        child: Text(
                            '${listFigurine[index].nom} et ${listFigurine[index].faction}')),
                    Expanded(
                        child: Checkbox(
                            value: inList[index],
                            onChanged: (value) => setState(() {
                                  inList[index] = value!;
                                })))
                  ],
                ));
          }),
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.check,
          ),
          onPressed: () {
            listFigurine.asMap().forEach((index, value) {
              if (inList[index]) listFigurineCheck.add(listFigurine[index]);
            });
            log(listFigurineCheck.toString());
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ActionsFight(listFigurine: listFigurineCheck)));
          }),

      //Afficher la liste des figurines
      //https://blogs.infinitesquare.com/posts/mobile/utiliser-les-listes-dans-vos-applications-flutter
    );
  }
}

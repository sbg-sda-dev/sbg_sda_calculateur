import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/bloc/persistance/persistanceFigurine.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

import 'GestionFigurines.dart';

class CreateFigurine extends StatefulWidget {
  final FigurineStorage storage;
  final Figurine figurine;
  CreateFigurine({Key? key, required this.storage, required this.figurine})
      : super(key: key);
  @override
  _CreateFigurineState createState() => _CreateFigurineState();
}

class _CreateFigurineState extends State<CreateFigurine> {
  bool isHero = false;

  @override
  Widget build(BuildContext context) {
    //Color _colorBien = Colors.blueGrey[800];
    //Color _colorMal = Colors.blueGrey[800];
    return Scaffold(
        //======================================================AppBar=========================================
        appBar: BaseAppBar(
          title: Text("Création d'une figurine"),
          appBar: AppBar(),
        ),

        //=============================================BODY================================================================
        body: Column(children: [
          Expanded(
              child: ListView(children: [
                //====================Valeur en point====================
                /*TextField(
              keyboardType: TextInputType.number,
              maxLength: 3,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration:
                  InputDecoration(labelText: "Valeur en point de la figurine"),
              onChanged: (_force) =>
                  {widget.figurine.force = int.tryParse(_force)}),*/
                //========================name=======================
                ButtonBar(
                  alignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    /*
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: _colorBien,
                      ),
                      onPressed: () {
                        widget.figurine.catForce = 'MAL';
                        setState(() {
                          _colorBien = Colors.green;
                        });
                      },
                      child: Text('BIEN'),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: _colorMal,
                      ),
                      onPressed: () => {widget.figurine.catForce = 'MAL'},
                      child: Text('MAL'),
                    ),*/
                    Text("BIEN"),
                    Switch(
                        value:
                            widget.figurine.catForce == "BIEN" ? true : false,
                        onChanged: (value) => setState(() {
                              widget.figurine.catForce = value ? "BIEN" : "MAL";
                            })),
                    Text("MAL"),
                  ],
                ),
                TextField(
                    keyboardType: TextInputType.name,
                    //inputFormatters: [FilteringTextInputFormatter.singleLineFormatter],
                    decoration: InputDecoration(
                      labelText: "Nom de la figurine",
                    ),
                    onChanged: (_nom) => {widget.figurine.nom = _nom}),
                //=========================image=============================
                /*TextField(
              keyboardType: TextInputType.number,
              maxLength: 3,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(labelText: "image de la figurine"),
              onChanged: (_attArc) =>
                  {widget.figurine.force = int.tryParse(_attArc)}),*/
                //==============================MV mouvement========================
                /*TextField(
              keyboardType: TextInputType.number,
              maxLength: 2,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(
                  labelText: "distance de mouvement de la figurine"),
              onChanged: (_attArc) =>
                  {widget.figurine.force = int.tryParse(_attArc)}),*/
                //===========================C Combat============================
                TextField(
                    keyboardType: TextInputType.number,
                    maxLength: 2,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    decoration: InputDecoration(
                        labelText: "Valeur de combat de la figurine"),
                    onChanged: (_combat) =>
                        {widget.figurine.combat = int.tryParse(_combat)!}),
                //==================F force========================
                TextField(
                    keyboardType: TextInputType.number,
                    maxLength: 2,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    decoration: InputDecoration(
                        labelText: "Valeur de force de la figurine"),
                    onChanged: (_force) =>
                        {widget.figurine.force = int.tryParse(_force)!}),
                //==============D défense======================================
                TextField(
                    keyboardType: TextInputType.number,
                    maxLength: 2,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    decoration: InputDecoration(
                        labelText: "Valeur de défense de la figurine"),
                    onChanged: (_defense) =>
                        {widget.figurine.defense = int.tryParse(_defense)!}),
                //===========================A attaque============================
                TextField(
                    keyboardType: TextInputType.number,
                    maxLength: 2,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    decoration: InputDecoration(
                        labelText: "Valeur d'attaque de la figurine"),
                    onChanged: (_attaque) =>
                        {widget.figurine.attaque = int.tryParse(_attaque)!}),
                //================PV point vie====================================
                /*TextField(
              keyboardType: TextInputType.number,
              maxLength: 1,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration:
                  InputDecoration(labelText: "Point de vie de la figurine"),
              onChanged: (_attArc) =>
                  {widget.figurine.force = int.tryParse(_attArc)}),*/
                //==================B bravoure=====================================
                /*TextField(
              keyboardType: TextInputType.number,
              maxLength: 2,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(labelText: "Bravoure de la figurine"),
              onChanged: (_attArc) =>
                  {widget.figurine.force = int.tryParse(_attArc)}),*/
                //==============================Héro booleen=================
                /*Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("Le personnage est un héro"),
              Checkbox(
                  value: this.isHero,
                  onChanged: (bool value) {
                    setState(() {
                      this.isHero = value;
                    });
                  }),
            ],
          ),*/
                if (isHero)
                  //================P puissance==================================
                  TextField(
                      keyboardType: TextInputType.number,
                      maxLength: 2,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                          labelText: "Valeur de puissance de la figurine"),
                      onChanged: (_attArc) =>
                          {widget.figurine.force = int.tryParse(_attArc)!}),
                if (isHero)
                  //==========================V volonté=====================
                  TextField(
                      keyboardType: TextInputType.number,
                      maxLength: 2,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                          labelText: "Point de volonté de la figurine"),
                      onChanged: (_attArc) =>
                          {widget.figurine.force = int.tryParse(_attArc)!}),
                if (isHero)
                  //================================D destin======================
                  TextField(
                      keyboardType: TextInputType.number,
                      maxLength: 2,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                          labelText: "Point de destin de la figurine"),
                      onChanged: (_attArc) =>
                          {widget.figurine.force = int.tryParse(_attArc)!})
              ]),
              flex: 8),
          Expanded(
              child: FloatingActionButton(
                onPressed: () {
                  widget.storage.readFigurine().then((value) {
                    log('=CreateFigurine= READ BEFORE ADDD =>$value');
                  });
                  widget.storage.addFigurine(widget.figurine);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              GestionFigurines(storage: FigurineStorage())));
                },
                tooltip: 'Save',
                child: Icon(Icons.save_alt),
              ),
              flex: 2)
        ]));
  }
}

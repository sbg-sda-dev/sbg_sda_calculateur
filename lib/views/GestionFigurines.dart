import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/bloc/persistance/persistanceFigurine.dart';
import 'package:sbg_sda_calculateur/views/CreateFigurine.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

class GestionFigurines extends StatefulWidget {
  final FigurineStorage storage;
  GestionFigurines({Key? key, required this.storage}) : super(key: key);

  @override
  GestionFigurinesScreenState createState() => GestionFigurinesScreenState();
}

class GestionFigurinesScreenState extends State<GestionFigurines> {
  List<Figurine> listFigurine = [];

  @override
  void initState() {
    super.initState();
    widget.storage.readFigurine().then((value) {
      setState(() {
        this.listFigurine += value;
      });
      log('Gestion de figurine -> value -> $value');
    });
  }

  @override
  Widget build(BuildContext context) {
    log('Gestion de figurine -> read -> $listFigurine');
    return Scaffold(
      //======================================================AppBar=========================================
      appBar: BaseAppBar(
        title: Text('Gestion de figurine'),
        appBar: AppBar(),
      ),

      //=======================================Body==================
      body: ListView.builder(
          itemCount: listFigurine.length,
          itemBuilder: (context, index) {
            return Container(
                margin: const EdgeInsets.only(bottom: 20.0, top: 20.0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                child: Row(
                  children: [
                    Icon(Icons.account_circle),
                    Text(
                        '${listFigurine[index].nom} et ${listFigurine[index].faction}'),
                    //IconButton(icon: Icon(Icons.share)),
                    IconButton(
                        icon: Icon(Icons.create),
                        onPressed: () {
                          showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => Scaffold(
                                    //======================================================AppBar=========================================
                                    appBar: BaseAppBar(
                                      title: Text("modifier figurine"),
                                      appBar: AppBar(),
                                    ),
                                  ));
                          /*Navigator.push(
                              context,
                              MaterialPageRoute(
                                  fullscreenDialog: true,
                                  builder: (context) => ModifyFigurine(
                                      storage: FigurineStorage(),
                                      figurine: listFigurine[index])));*/
                        }),
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () => setState(() {
                        this.listFigurine.removeAt(index);
                        widget.storage.writeFigurine(listFigurine);
                      }),
                    ),
                  ],
                ));
          }),
      //-----------------Bouton d'ajout de figurine---------------------
      floatingActionButton: FloatingActionButton(
          heroTag: 'Fabtn_AddFigurine',
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    fullscreenDialog: true,
                    builder: (context) => CreateFigurine(
                        storage: FigurineStorage(),
                        figurine: Figurine.autogen())));
          }),

      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: FloatingActionButton(
            heroTag: 'Fabtn_RestFigurine',
            child: Icon(
              Icons.delete,
              semanticLabel: "HARD RESET",
            ),
            backgroundColor: Colors.red,
            onPressed: () {
              widget.storage.resetFigurine();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          GestionFigurines(storage: FigurineStorage())));
            }),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}
//==================================================================
/*class _ModifyFigurine extends State<ModifyFigurine> {
  bool isHero = false;

  //---------https://pub.dev/packages/image_picker---------
  File _image;
  final picker = ImagePicker();
  get child => null;
  Future getImageCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listCompetenceFigurine = [
      Text("C \n (corp à corp)"),
      Text("C \n (tir à l'arc)"),
      Text("F"),
      Text("D"),
      Text("A"),
      Text("PV"),
      Text("B"),
      Text(widget.figurine.combat.toString()),
      Text(widget.figurine.combatTir.toString()),
      Text(widget.figurine.force.toString()),
      Text(widget.figurine.defense.toString()),
      Text(widget.figurine.attaque.toString()),
      TextField(
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          maxLength: 3,
          style: Theme.of(context).textTheme.headline4,
          decoration: InputDecoration(
              labelText: '0',
              //errorText: _numberInputIsValid ? null : 'Please enter an integer!',
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ))),
      Text("0"),
    ];
    //Color _colorBien = Colors.blueGrey[800];
    //Color _colorMal = Colors.blueGrey[800];
    return Scaffold(
        //======================================================AppBar=========================================
        appBar: BaseAppBar(
          title: Text("modifier figurine"),
          appBar: AppBar(),
        ),

        //=============================================BODY================================================================
        body: Column(children: <Widget>[
          ButtonBar(
            alignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.figurine.nom,
              ),
              Text(widget.figurine.faction)
            ],
          ),
          Divider(
            indent: 50,
            endIndent: 50,
          ),
          Container(
            height: 200,
            width: MediaQuery.of(context).size.width * 0.90,
            child: _image == null
                ? OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.grey[700]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.photo_camera),
                        Text("ajouter une image")
                      ],
                    ),
                    onPressed: () {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => SimpleDialog(
                                title: Text("mode image"),
                                children: [
                                  TextButton(
                                      onPressed: getImageCamera,
                                      child: const Text("Camera")),
                                  TextButton(
                                      onPressed: getImageGallery,
                                      child: const Text("Gallery"))
                                ],
                              ));
                    },
                  )
                : Image.file(_image),
          ),
          Divider(
            indent: 50,
            endIndent: 50,
          ),
          Expanded(
              flex: 1,
              child: new GridView.count(
                  //physics: BouncingScrollPhysics(),
                  crossAxisCount: 7,
                  children: listCompetenceFigurine)),
          /*Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("data"),
              Text("data"),
              Text("data"),
              Text("data"),
              Text("data"),
            ],
          ),
          Divider(
            color: Colors.amber,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(""),
              Text("data"),
              Text("data"),
              Text("data"),
              Text("data"),
            ],
          )*/
        ]));
  }
}*/

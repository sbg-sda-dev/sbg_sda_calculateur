import 'package:flutter/material.dart';
import 'package:sbg_sda_calculateur/bloc/persistance/persistanceFigurine.dart';
import 'package:sbg_sda_calculateur/constants.dart';
import 'package:sbg_sda_calculateur/responsive.dart';
import 'package:sbg_sda_calculateur/views/GestionFigurines.dart';
import 'package:sbg_sda_calculateur/views/Liste_armee.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //======================================================AppBar=========================================
      appBar: BaseAppBar(
        title: Text('Home'),
        appBar: AppBar(),
        isHome: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
              child: ElevatedButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultPadding * 1.5,
                      vertical: defaultPadding /
                          (Responsive.isMobile(context) ? 2 : 1),
                    ),
                  ),
                  autofocus: true,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ListeArmee(storage: FigurineStorage())));
                  },
                  child: Text("Créer une liste d'armée"))),
          Center(
              child: ElevatedButton(
                  autofocus: true,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                GestionFigurines(storage: FigurineStorage())));
                  },
                  child: Text("Gérer mes figurines")))
        ],
      ),
    );
  }
}

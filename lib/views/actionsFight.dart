import 'package:flutter/material.dart';
import 'package:sbg_sda_calculateur/backend/Figurines.dart';
import 'package:sbg_sda_calculateur/views/tir_arc.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

import 'combat.dart';

class ActionsFight extends StatefulWidget {
  final List<Figurine> listFigurine;
  ActionsFight({
    Key? key,
    required this.listFigurine,
  }) : super(key: key);

  @override
  ActionsFightScreenState createState() => ActionsFightScreenState();
}

class ActionsFightScreenState extends State<ActionsFight> {
  List<Figurine> listFigurine = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      this.listFigurine = widget.listFigurine;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //======================================================AppBar=========================================
      appBar: BaseAppBar(
        title: Text("Liste d'armée"),
        appBar: AppBar(),
      ),

      //=======================================Body==================
      body: ListView(
        children: [
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TirArc(
                              listFigurine: [],
                            )));
              },
              child: Text("Tirer")),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Combat()));
              },
              child: Text("Combat"))
        ],
      ),
    );
  }
}

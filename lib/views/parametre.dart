import 'package:flutter/material.dart';
import 'package:sbg_sda_calculateur/widget/BaseAppBar.dart';

class ParametreScreen extends StatefulWidget {
  ParametreScreen({Key? key}) : super(key: key);

  @override
  ParametreScreenState createState() => ParametreScreenState();
}

class ParametreScreenState extends State<ParametreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //======================================================AppBar=========================================
      appBar: BaseAppBar(
        title: Text('Parametre'),
        appBar: AppBar(),
      ),
    );
  }
}
